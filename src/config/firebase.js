import firebase from 'firebase/app';
import 'firebase/database';

const firebaseConfig = {
    apiKey: "AIzaSyDrR6-5yP1CejFw9dSBI4h2r-3nIAPOaGY",
    authDomain: "ehealth-5e128.firebaseapp.com",
    databaseURL: "https://ehealth-5e128.firebaseio.com",
    projectId: "ehealth-5e128",
    storageBucket: "ehealth-5e128.appspot.com",
    messagingSenderId: "910821073427",
    appId: "1:910821073427:web:c1eec0bf7c371005b0d5e6"
  };

 let firebaseInstance = firebase.initializeApp(firebaseConfig)

 let firebase_database = firebaseInstance.database()

 export default  firebase_database