//global highchart :options settings for each chart { age_chart, and blood_group }

let chartOptions = {
    age_distribution:{
        xAxis: {
            categories: [ 
            '0-10',
            '11-20',
            '21-30',
            '31-40',
            '41-50', ],
            crosshair: true,
            title: {
              text: "Age distribution amongs participant"
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            allowPointSelect: true,
            borderRadius: 20
        },
        tooltip: {
            valueSuffix: ' participants'
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Participants age count',
                align: 'middle'
            },
        },
        title:{
                text: 'Age distribution <br>(Online data)</br>'
            },
            subtitle: {
                text: 'Source "Firebase database"'
            },
            chart:{
            type: 'column'
        },
        credits:{
            enabled: false
        },
    },

    blood_group:{
        xAxis: {
          categories: [ "Blood group" ],
          title: {
            text: null
          }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total data (20)',
                align: 'high'
            },
        },
        plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
        },
        legend: {
              reversed: false,
              borderWidth: 1,
              shadow: true
          },
        tooltip: {
          valueSuffix: ' participants'
         },
        title:{
          text: 'Blood groups <br>(Online data)</br>'
        },
         subtitle: {
        text: 'Source "Firebase database"'
        },
        chart:{
          type: 'bar'
        },
        
        credits:{
          enabled: false
        },
      },
      
}

export { chartOptions } 